EESchema Schematic File Version 4
LIBS:sifflet-only-lora-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4950 1050 4950 1000
Wire Wire Line
	4950 1000 4850 1000
Wire Wire Line
	4850 1000 4850 1050
Wire Wire Line
	4850 1000 4750 1000
Wire Wire Line
	4750 1000 4750 1050
Connection ~ 4850 1000
Wire Wire Line
	4750 1000 4650 1000
Wire Wire Line
	4650 1000 4650 1050
Connection ~ 4750 1000
Wire Wire Line
	4650 1000 4550 1000
Wire Wire Line
	4550 1000 4550 1050
Connection ~ 4650 1000
Wire Wire Line
	4550 1000 4150 1000
Wire Wire Line
	4150 1000 4150 1450
Wire Wire Line
	4150 1450 4200 1450
Connection ~ 4550 1000
Wire Wire Line
	4150 1450 4150 1550
Wire Wire Line
	4150 1550 4200 1550
Connection ~ 4150 1450
Wire Wire Line
	4150 1550 4150 1650
Wire Wire Line
	4150 1650 4200 1650
Connection ~ 4150 1550
Wire Wire Line
	4150 1650 4150 1750
Wire Wire Line
	4150 1750 4200 1750
Connection ~ 4150 1650
$Comp
L power:GND #PWR0101
U 1 1 5E176BDA
P 4150 1750
F 0 "#PWR0101" H 4150 1500 50  0001 C CNN
F 1 "GND" H 4155 1577 50  0001 C CNN
F 2 "" H 4150 1750 50  0001 C CNN
F 3 "" H 4150 1750 50  0001 C CNN
	1    4150 1750
	1    0    0    -1  
$EndComp
Connection ~ 4150 1750
Wire Wire Line
	4200 2850 4150 2850
$Comp
L Device:C_Small C8
U 1 1 5E17B28C
P 5950 5000
F 0 "C8" H 6000 5250 50  0000 R BNN
F 1 "1uF" H 6000 5350 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5950 5000 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 5950 5000 50  0001 C CNN
F 4 "muRata" H 5950 5000 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 5950 5000 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 5950 5000 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5950 5000 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 5950 5000 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 5950 5000 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 5950 5000 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5950 5000 50  0001 C CNN "DeviceFournisseur"
	1    5950 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5E17CC27
P 6100 5000
F 0 "C9" H 6100 5150 50  0000 R BNN
F 1 "0.1uF" H 6050 5250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6100 5000 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 6100 5000 50  0001 C CNN
F 4 "muRata" H 6100 5000 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 6100 5000 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 6100 5000 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6100 5000 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 6100 5000 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 6100 5000 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 6100 5000 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6100 5000 50  0001 C CNN "DeviceFournisseur"
	1    6100 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5E17E7E2
P 5450 5000
F 0 "C5" H 5500 5150 50  0000 R BNN
F 1 "10uF" H 5550 5250 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5450 5000 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R60J106KE47-01A.pdf" H 5450 5000 50  0001 C CNN
F 4 "muRata" H 5450 5000 50  0001 C CNN "Fabricant"
F 5 "GRM188R60J106KE47D" H 5450 5000 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R60J106KE47%23.html" H 5450 5000 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5450 5000 50  0001 C CNN "Fournisseur"
F 8 "2494230" H 5450 5000 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2494230" H 5450 5000 50  0001 C CNN "LinkFournisseur"
F 10 "0,148" H 5450 5000 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5450 5000 50  0001 C CNN "DeviceFournisseur"
	1    5450 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5E17F1DB
P 5600 5000
F 0 "C6" H 5650 5250 50  0000 R BNN
F 1 "0.1uF" H 5600 5350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5600 5000 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 5600 5000 50  0001 C CNN
F 4 "muRata" H 5600 5000 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 5600 5000 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 5600 5000 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5600 5000 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 5600 5000 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 5600 5000 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 5600 5000 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5600 5000 50  0001 C CNN "DeviceFournisseur"
	1    5600 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5E1829A7
P 5950 900
F 0 "R6" H 5900 750 50  0000 L CNN
F 1 "10k" V 5950 900 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5950 900 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 5950 900 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 5950 900 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 5950 900 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 5950 900 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5950 900 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 5950 900 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 5950 900 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 5950 900 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5950 900 50  0001 C CNN "DeviceFournisseur"
	1    5950 900 
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 5100 5600 5100
Wire Wire Line
	4200 2650 4150 2650
Wire Wire Line
	4150 2650 4150 2750
Wire Wire Line
	4150 2750 4200 2750
Wire Wire Line
	5450 4900 5600 4900
Wire Wire Line
	5800 4900 5950 4900
Connection ~ 5950 4900
Wire Wire Line
	5950 4900 6100 4900
Wire Wire Line
	6100 5100 5950 5100
Connection ~ 5600 5100
Connection ~ 5800 5100
Wire Wire Line
	5800 5100 5600 5100
Connection ~ 5950 5100
Wire Wire Line
	5950 5100 5800 5100
Wire Wire Line
	6100 5100 6250 5100
Connection ~ 6100 5100
$Comp
L power:GND #PWR0102
U 1 1 5E1970B7
P 6250 5100
F 0 "#PWR0102" H 6250 4850 50  0001 C CNN
F 1 "GND" H 6255 4927 50  0001 C CNN
F 2 "" H 6250 5100 50  0001 C CNN
F 3 "" H 6250 5100 50  0001 C CNN
	1    6250 5100
	1    0    0    -1  
$EndComp
Text Label 5800 4800 0    50   ~ 0
VDD
Wire Wire Line
	4200 2450 4150 2450
$Comp
L sifflet:CMWX1ZZABZ-091 U3
U 1 1 5E16F702
P 5750 2550
F 0 "U3" H 7250 3900 50  0000 L CNN
F 1 "CMWX1ZZABZ-091" H 7250 3800 50  0000 L CNN
F 2 "sifflet:MODULE_CMWX1ZZABZ-091" H 5750 3950 50  0001 C CNN
F 3 "https://wireless.murata.com/pub/RFM/data/type_abz.pdf" H 5750 3950 50  0001 L CNN
F 4 "muRata" H 4950 550 50  0001 L CNN "Fabricant"
F 5 "CMWX1ZZABZ-091" H 4950 450 50  0001 L CNN "RefFabricant"
F 6 "https://wireless.murata.com/type-abz.html" H 4950 350 50  0001 L CNN "LinkFabricant"
F 7 "Farnell" H 4950 250 50  0001 L CNN "Fournisseur"
F 8 "2802546" H 4950 150 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2802546" H 4950 50  50  0001 L CNN "LinkFournisseur"
F 10 "16,72" H 4950 -50 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 4950 -150 50  0001 L CNN "DeviceFournisseur"
	1    5750 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E177ED9
P 4150 2450
F 0 "#PWR0103" H 4150 2200 50  0001 C CNN
F 1 "GND" H 4155 2277 50  0001 C CNN
F 2 "" H 4150 2450 50  0001 C CNN
F 3 "" H 4150 2450 50  0001 C CNN
	1    4150 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E17832A
P 4150 2850
F 0 "#PWR0104" H 4150 2600 50  0001 C CNN
F 1 "GND" H 4155 2677 50  0001 C CNN
F 2 "" H 4150 2850 50  0001 C CNN
F 3 "" H 4150 2850 50  0001 C CNN
	1    4150 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E19FE69
P 5750 800
F 0 "#PWR0105" H 5750 550 50  0001 C CNN
F 1 "GND" H 5755 627 50  0001 C CNN
F 2 "" H 5750 800 50  0001 C CNN
F 3 "" H 5750 800 50  0001 C CNN
	1    5750 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 800  5850 800 
Wire Wire Line
	5850 800  5850 1050
Connection ~ 5850 800 
Wire Wire Line
	5850 800  5750 800 
Wire Wire Line
	5950 1000 5950 1050
Wire Wire Line
	7300 2900 7350 2900
Wire Wire Line
	7350 3100 7300 3100
Text Label 5450 900  2    50   ~ 0
VDD_TCXO
Text Label 5600 4800 2    50   ~ 0
VDD_USD
$Comp
L power:GND #PWR0106
U 1 1 5E1B3383
P 7350 3100
F 0 "#PWR0106" H 7350 2850 50  0001 C CNN
F 1 "GND" H 7355 2927 50  0001 C CNN
F 2 "" H 7350 3100 50  0001 C CNN
F 3 "" H 7350 3100 50  0001 C CNN
	1    7350 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E1B3CF8
P 7350 2900
F 0 "#PWR0107" H 7350 2650 50  0001 C CNN
F 1 "GND" H 7355 2727 50  0001 C CNN
F 2 "" H 7350 2900 50  0001 C CNN
F 3 "" H 7350 2900 50  0001 C CNN
	1    7350 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5E1C0070
P 7600 2500
F 0 "C10" H 7850 2450 50  0000 R CNN
F 1 "1uF" H 7850 2500 50  0000 R BNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7600 2500 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 7600 2500 50  0001 C CNN
F 4 "muRata" H 7600 2500 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 7600 2500 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 7600 2500 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 7600 2500 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 7600 2500 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 7600 2500 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 7600 2500 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 7600 2500 50  0001 C CNN "DeviceFournisseur"
	1    7600 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7300 2400 7600 2400
Text Label 7600 2400 0    50   ~ 0
VREF+
Text Label 7900 2400 0    50   ~ 0
LORA_VDD
Wire Wire Line
	7600 2600 7600 2650
Wire Wire Line
	7900 2400 7600 2400
Connection ~ 7600 2400
$Comp
L power:GND #PWR0108
U 1 1 5E1C4525
P 7600 2650
F 0 "#PWR0108" H 7600 2400 50  0001 C CNN
F 1 "GND" H 7605 2477 50  0001 C CNN
F 2 "" H 7600 2650 50  0001 C CNN
F 3 "" H 7600 2650 50  0001 C CNN
	1    7600 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 900  5450 1050
$Comp
L sifflet:L70B-M39 U1
U 1 1 5E18D188
P 2500 6800
F 0 "U1" H 2500 7425 50  0000 C CNN
F 1 "L70B-M39" H 2500 6100 50  0000 C CNN
F 2 "" H 2750 6550 50  0001 C CNN
F 3 "https://www.quectel.com/UploadImage/Downlad/Quectel_L70_Hardware_Design_V2.2.pdf" H 2750 6550 50  0001 L CNN
F 4 "Quectel" H 3250 7150 50  0001 L CNN "Fabricant"
F 5 "L70B-M39" H 3250 7050 50  0001 L CNN "RefFabricant"
F 6 "https://www.quectel.com/UploadFile/Product/Quectel_L70_GPS_Specification_V2.3.pdf" H 3250 6950 50  0001 L CNN "LinkFabricant"
F 7 "AliExpress" H 3250 6850 50  0001 L CNN "Fournisseur"
F 8 "32970071689" H 3250 6750 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/32970071689.html" H 3250 6650 50  0001 L CNN "LinkFournisseur"
F 10 "5,95" H 3250 6550 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 3250 6450 50  0001 L CNN "DeviceFournisseur"
	1    2500 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E1DDC6D
P 3100 7500
F 0 "#PWR0109" H 3100 7250 50  0001 C CNN
F 1 "GND" H 3105 7327 50  0001 C CNN
F 2 "" H 3100 7500 50  0001 C CNN
F 3 "" H 3100 7500 50  0001 C CNN
	1    3100 7500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5E20A18D
P 1550 6850
F 0 "R3" H 1600 6900 50  0000 L BNN
F 1 "10k" H 1550 6850 13  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1550 6850 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 1550 6850 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 1550 6850 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 1550 6850 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 1550 6850 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1550 6850 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 1550 6850 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 1550 6850 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 1550 6850 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1550 6850 50  0001 C CNN "DeviceFournisseur"
	1    1550 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 7400 3100 7400
Wire Wire Line
	3100 7400 3100 7500
Wire Wire Line
	3050 7300 3100 7300
Wire Wire Line
	3100 7300 3100 7400
Connection ~ 3100 7400
Wire Wire Line
	3050 7200 3100 7200
Wire Wire Line
	3100 7200 3100 7300
Connection ~ 3100 7300
Text Label 1900 6700 2    50   ~ 0
VCCRF
$Comp
L power:GND #PWR0110
U 1 1 5E1E250D
P 1350 7650
F 0 "#PWR0110" H 1350 7400 50  0001 C CNN
F 1 "GND" H 1355 7477 50  0001 C CNN
F 2 "" H 1350 7650 50  0001 C CNN
F 3 "" H 1350 7650 50  0001 C CNN
	1    1350 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_SGD Q2
U 1 1 5E2493D9
P 1450 7400
F 0 "Q2" H 1650 7300 50  0000 L CNN
F 1 "Q_PMOS_SGD" V 1702 7400 50  0001 C CNN
F 2 "" H 1650 7500 50  0001 C CNN
F 3 "~" H 1450 7400 50  0001 C CNN
	1    1450 7400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1550 6700 1550 6750
Wire Wire Line
	1350 7650 1350 7600
Wire Wire Line
	1150 6700 1100 6700
$Comp
L Device:R_Small R1
U 1 1 5E27ED73
P 1000 6700
F 0 "R1" V 900 6650 50  0000 L CNN
F 1 "10k" V 1000 6700 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1000 6700 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 1000 6700 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 1000 6700 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 1000 6700 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 1000 6700 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1000 6700 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 1000 6700 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 1000 6700 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 1000 6700 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1000 6700 50  0001 C CNN "DeviceFournisseur"
	1    1000 6700
	0    1    1    0   
$EndComp
$Comp
L Device:L_Small L1
U 1 1 5E289D69
P 750 6700
F 0 "L1" V 900 6700 50  0000 C BNN
F 1 "47nH" V 800 6700 50  0000 C CNN
F 2 "" H 750 6700 50  0001 C CNN
F 3 "~" H 750 6700 50  0001 C CNN
	1    750  6700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	900  6700 850  6700
Wire Wire Line
	650  6700 600  6700
$Comp
L power:GND #PWR0111
U 1 1 5E29D776
P 1200 6550
F 0 "#PWR0111" H 1200 6300 50  0001 C CNN
F 1 "GND" H 1205 6377 50  0001 C CNN
F 2 "" H 1200 6550 50  0001 C CNN
F 3 "" H 1200 6550 50  0001 C CNN
	1    1200 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6550 1200 6500
$Comp
L power:GND #PWR0112
U 1 1 5E29FDDF
P 1500 6550
F 0 "#PWR0112" H 1500 6300 50  0001 C CNN
F 1 "GND" H 1505 6377 50  0001 C CNN
F 2 "" H 1500 6550 50  0001 C CNN
F 3 "" H 1500 6550 50  0001 C CNN
	1    1500 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6550 1500 6500
Wire Wire Line
	600  6300 600  6700
Wire Wire Line
	600  6300 600  6250
Connection ~ 600  6300
$Comp
L Device:Antenna AE1
U 1 1 5E2B360E
P 600 6050
F 0 "AE1" H 680 6039 50  0000 L CNN
F 1 "Active Antenna" H 680 5948 50  0000 L CNN
F 2 "" H 600 6050 50  0001 C CNN
F 3 "~" H 600 6050 50  0001 C CNN
	1    600  6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5E2B78EB
P 1500 6400
F 0 "C2" H 1600 6400 50  0000 L CNN
F 1 "NM" H 1600 6300 50  0000 L CNN
F 2 "" H 1500 6400 50  0001 C CNN
F 3 "~" H 1500 6400 50  0001 C CNN
	1    1500 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5E2B7FE2
P 1200 6400
F 0 "C1" H 1300 6400 50  0000 L CNN
F 1 "NM" H 1300 6300 50  0000 L CNN
F 2 "" H 1200 6400 50  0001 C CNN
F 3 "~" H 1200 6400 50  0001 C CNN
	1    1200 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5E2D59C7
P 1350 6300
F 0 "R2" V 1250 6250 50  0000 L CNN
F 1 "0" V 1350 6300 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1350 6300 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDP0000/DMP0000COL17.pdf" H 1350 6300 50  0001 C CNN
F 4 "PANASONIC" H 1350 6300 50  0001 C CNN "Fabricant"
F 5 "ERJU030R00V" H 1350 6300 50  0001 C CNN "RefFabricant"
F 6 "https://industrial.panasonic.com/ww/products/resistors/chip-resistors/anti-sulfurated-chip-resistors/anti-sulfurated-thick-film-chip-resistors/ERJU030R00V" H 1350 6300 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1350 6300 50  0001 C CNN "Fournisseur"
F 8 "2797491" H 1350 6300 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2797491" H 1350 6300 50  0001 C CNN "LinkFournisseur"
F 10 "0,0523" H 1350 6300 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1350 6300 50  0001 C CNN "DeviceFournisseur"
	1    1350 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	600  6300 1200 6300
Wire Wire Line
	1500 6300 1450 6300
Connection ~ 1500 6300
Wire Wire Line
	1250 6300 1200 6300
Connection ~ 1200 6300
Text Label 1900 7300 2    50   ~ 0
GPS_TRX1
Text Label 1900 7200 2    50   ~ 0
GPS_RTX1
Wire Wire Line
	3350 6500 3350 6550
Wire Wire Line
	3350 6750 3350 6800
Wire Wire Line
	3150 6750 3150 6800
Wire Wire Line
	3150 6550 3150 6500
Wire Wire Line
	3150 6500 3350 6500
Wire Wire Line
	3150 6500 3050 6500
Connection ~ 3150 6500
Wire Wire Line
	3150 6800 3350 6800
Connection ~ 3350 6500
Connection ~ 3350 6800
Wire Wire Line
	3350 6800 3550 6800
$Comp
L power:GND #PWR0113
U 1 1 5E1A68F1
P 3550 6800
F 0 "#PWR0113" H 3550 6550 50  0001 C CNN
F 1 "GND" H 3555 6627 50  0001 C CNN
F 2 "" H 3550 6800 50  0001 C CNN
F 3 "" H 3550 6800 50  0001 C CNN
	1    3550 6800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3800 6500 3850 6500
Connection ~ 3550 6800
Wire Wire Line
	3600 6500 3550 6500
Wire Wire Line
	3350 6500 3550 6500
Connection ~ 3550 6500
$Comp
L Device:Battery_Cell BT1
U 1 1 5E19C52A
P 3550 6700
F 0 "BT1" H 3650 6700 50  0000 L CNN
F 1 "Battery_Cell" H 3668 6705 50  0001 L CNN
F 2 "" V 3550 6760 50  0001 C CNN
F 3 "~" V 3550 6760 50  0001 C CNN
	1    3550 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5E1988DC
P 3350 6650
F 0 "C4" H 3300 6550 50  0000 C CNN
F 1 "0.1uF" H 3300 6450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 6650 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 3350 6650 50  0001 C CNN
F 4 "muRata" H 3350 6650 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 3350 6650 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 3350 6650 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 3350 6650 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 3350 6650 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 3350 6650 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 3350 6650 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 3350 6650 50  0001 C CNN "DeviceFournisseur"
	1    3350 6650
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5E206ACA
P 3150 6650
F 0 "C3" H 3150 6550 50  0000 L CNN
F 1 "4.7uF" H 3300 6450 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3150 6650 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R61C475KAAJ-01A.pdf" H 3150 6650 50  0001 C CNN
F 4 "muRata" H 3150 6650 50  0001 C CNN "Fabricant"
F 5 "GRM188R61C475KAAJD" H 3150 6650 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R61C475KAAJ%23.html" H 3150 6650 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 3150 6650 50  0001 C CNN "Fournisseur"
F 8 "2611924" H 3150 6650 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2611924" H 3150 6650 50  0001 C CNN "LinkFournisseur"
F 10 "0,305" H 3150 6650 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 3150 6650 50  0001 C CNN "DeviceFournisseur"
	1    3150 6650
	1    0    0    -1  
$EndComp
Text Notes 4550 7750 0    50   ~ 0
9.2.2 Detailed Design Procedure -\nhttp://www.ti.com/lit/ds/symlink/tps22918-q1.pdf#page=17\n\n9.2.2.1 Input Capacitor\nCin=Cout*10\nCin>=1uF\n\n6.6 Switching Characteristics -\nhttp://www.ti.com/lit/ds/symlink/tps22918-q1.pdf#page=6\nRL= 10Ω, CIN= 1 μF, COUT= 0.1 μF, CT = 1000pF
$Comp
L Device:C_Small Cin1
U 1 1 5E43A25F
P 6050 6550
F 0 "Cin1" H 6300 6500 50  0000 R CNN
F 1 "1uF" H 6300 6550 50  0000 R BNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6050 6550 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 6050 6550 50  0001 C CNN
F 4 "muRata" H 6050 6550 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 6050 6550 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 6050 6550 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6050 6550 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 6050 6550 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 6050 6550 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 6050 6550 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6050 6550 50  0001 C CNN "DeviceFournisseur"
	1    6050 6550
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5E460388
P 6050 6700
F 0 "#PWR0114" H 6050 6450 50  0001 C CNN
F 1 "GND" H 6055 6527 50  0001 C CNN
F 2 "" H 6050 6700 50  0001 C CNN
F 3 "" H 6050 6700 50  0001 C CNN
	1    6050 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 6650 6050 6700
$Comp
L Device:C_Small Cl/out1
U 1 1 5E491B5B
P 4800 6350
F 0 "Cl/out1" H 4900 6300 50  0000 L CNN
F 1 "0.1uF" H 4900 6350 50  0000 L BNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4800 6350 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 4800 6350 50  0001 C CNN
F 4 "muRata" H 4800 6350 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 4800 6350 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 4800 6350 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 4800 6350 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 4800 6350 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 4800 6350 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 4800 6350 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 4800 6350 50  0001 C CNN "DeviceFournisseur"
	1    4800 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 6650 5250 6650
Wire Wire Line
	4950 6250 4800 6250
Connection ~ 4950 6250
Wire Wire Line
	4800 6450 4950 6450
Connection ~ 4950 6450
$Comp
L power:GND #PWR0115
U 1 1 5E4C4D35
P 4800 6700
F 0 "#PWR0115" H 4800 6450 50  0001 C CNN
F 1 "GND" H 4805 6527 50  0001 C CNN
F 2 "" H 4800 6700 50  0001 C CNN
F 3 "" H 4800 6700 50  0001 C CNN
	1    4800 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 6450 6050 6450
Connection ~ 6050 6450
Wire Wire Line
	6050 6450 5950 6450
Wire Wire Line
	5950 6650 6050 6650
Connection ~ 6050 6650
Text Label 6100 6450 0    50   ~ 0
LDO_3.3V
Wire Wire Line
	4800 6650 4950 6650
Wire Wire Line
	4800 6650 4800 6700
Connection ~ 4800 6650
$Comp
L Device:C_Small CT1
U 1 1 5E507B30
P 5050 6650
F 0 "CT1" V 4900 6650 50  0000 C TNN
F 1 "1000uF" V 4850 6800 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5050 6650 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM1885C1H102JA01-01A.pdf" H 5050 6650 50  0001 C CNN
F 4 "muRata" H 5050 6650 50  0001 C CNN "Fabricant"
F 5 "GRM1885C1H102JA01D" H 5050 6650 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM1885C1H102JA01%23.html" H 5050 6650 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5050 6650 50  0001 C CNN "Fournisseur"
F 8 "8819920" H 5050 6650 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=8819920" H 5050 6650 50  0001 C CNN "LinkFournisseur"
F 10 "0,0428" H 5050 6650 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5050 6650 50  0001 C CNN "DeviceFournisseur"
	1    5050 6650
	0    1    -1   0   
$EndComp
Wire Wire Line
	5250 6450 5150 6450
Wire Wire Line
	5250 6250 5150 6250
Connection ~ 5150 6250
Wire Wire Line
	5150 6250 4950 6250
Connection ~ 5150 6450
Wire Wire Line
	5150 6450 4950 6450
Wire Wire Line
	4800 6250 4750 6250
Connection ~ 4800 6250
Wire Wire Line
	4800 6450 4800 6650
Connection ~ 4800 6450
Wire Wire Line
	5950 6250 6050 6250
Connection ~ 6050 6250
Wire Wire Line
	6050 6250 6100 6250
Wire Wire Line
	6350 6250 6300 6250
Wire Wire Line
	1350 7000 1550 7000
$Comp
L Device:Q_PMOS_SGD Q1
U 1 1 5E24599B
P 1350 6800
F 0 "Q1" V 1250 6950 50  0000 C CNN
F 1 "Q_PMOS_SGD" V 1602 6800 50  0001 C CNN
F 2 "" H 1550 6900 50  0001 C CNN
F 3 "~" H 1350 6800 50  0001 C CNN
	1    1350 6800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 6950 1550 7000
Connection ~ 1550 6700
Wire Wire Line
	1350 7000 1350 7200
Connection ~ 1350 7000
Wire Wire Line
	1950 7200 1900 7200
Text Label 1900 6300 2    50   ~ 0
RFIN
Wire Wire Line
	1950 7300 1900 7300
Text Label 3100 6300 0    50   ~ 0
GPS_VCC
Text Label 3100 6400 0    50   ~ 0
GPS_TIMER
Wire Wire Line
	3100 6400 3050 6400
Wire Wire Line
	3100 6300 3050 6300
Text Label 3900 6250 0    50   ~ 0
LDO_3.3V
Wire Wire Line
	3900 6250 3850 6250
Wire Wire Line
	6300 6150 6350 6150
$Comp
L Device:D_Small D3
U 1 1 5E573B3D
P 6200 6150
F 0 "D3" H 6150 6100 50  0000 R CNN
F 1 "D_Small" H 6450 6250 50  0001 R CNN
F 2 "" V 6200 6150 50  0001 C CNN
F 3 "~" V 6200 6150 50  0001 C CNN
	1    6200 6150
	1    0    0    1   
$EndComp
Text Label 6350 6150 0    50   ~ 0
GPS_SS
Wire Wire Line
	6050 6150 6100 6150
Wire Wire Line
	6050 6250 6050 6150
Text Label 4750 6250 2    50   ~ 0
GPS_VCC
Text Notes 4300 5850 0    50   ~ 0
https://hackaday.io/project/35169-hackable-cmwx1zzabz-lora-devices\nhttps://wireless.murata.com/pub/RFM/data/type_abz.pdf#page=9\n
Text Label 6350 6250 0    50   ~ 0
GPS_TIMER
Wire Wire Line
	1900 7100 1950 7100
Text Label 1900 7400 2    50   ~ 0
ANTON
Text Label 1900 7100 2    50   ~ 0
GPS_STANDBY
Wire Wire Line
	1550 6700 1950 6700
Wire Wire Line
	1500 6300 1950 6300
Wire Wire Line
	1650 7400 1950 7400
$Comp
L Device:R_Small R5
U 1 1 5E7208D5
P 3700 6950
F 0 "R5" V 3800 6900 50  0000 L BNN
F 1 "1k" V 3700 6950 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 6950 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 3700 6950 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 3700 6950 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 3700 6950 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 3700 6950 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 3700 6950 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 3700 6950 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 3700 6950 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 3700 6950 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 3700 6950 50  0001 C CNN "DeviceFournisseur"
	1    3700 6950
	0    1    -1   0   
$EndComp
$Comp
L Device:LED_Small D2
U 1 1 5E728E71
P 3850 7100
F 0 "D2" H 3850 7000 50  0000 C CNN
F 1 "LED_Small" H 3850 7244 50  0001 C CNN
F 2 "" V 3850 7100 50  0001 C CNN
F 3 "~" V 3850 7100 50  0001 C CNN
	1    3850 7100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3800 6950 3850 6950
$Comp
L Device:D_Small D1
U 1 1 5E1A92A1
P 3850 6350
F 0 "D1" H 3900 6450 50  0000 R TNN
F 1 "D_Small" H 4100 6450 50  0001 R CNN
F 2 "" V 3850 6350 50  0001 C CNN
F 3 "~" V 3850 6350 50  0001 C CNN
	1    3850 6350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5E192B8A
P 3700 6500
F 0 "R4" V 3800 6450 50  0000 L BNN
F 1 "1k" V 3700 6500 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3700 6500 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 3700 6500 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 3700 6500 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 3700 6500 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 3700 6500 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 3700 6500 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 3700 6500 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 3700 6500 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 3700 6500 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 3700 6500 50  0001 C CNN "DeviceFournisseur"
	1    3700 6500
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E70EB7E
P 3850 7250
F 0 "#PWR0116" H 3850 7000 50  0001 C CNN
F 1 "GND" H 3855 7077 50  0001 C CNN
F 2 "" H 3850 7250 50  0001 C CNN
F 3 "" H 3850 7250 50  0001 C CNN
	1    3850 7250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 7200 3850 7250
Text Label 3100 7050 0    50   ~ 0
GPS_RESET
Wire Wire Line
	3050 6950 3600 6950
Wire Wire Line
	3850 6450 3850 6500
Wire Wire Line
	3850 6950 3850 7000
Wire Wire Line
	3100 7050 3050 7050
Text Label 3100 6950 0    50   ~ 0
GPS_1PPS
$Comp
L Device:R_Small R7
U 1 1 5E53D405
P 6050 6350
F 0 "R7" H 6100 6300 50  0000 L BNN
F 1 "470k" V 6050 6350 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6050 6350 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 6050 6350 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 6050 6350 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J470K" H 6050 6350 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 6050 6350 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6050 6350 50  0001 C CNN "Fournisseur"
F 8 "2861880" H 6050 6350 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861880" H 6050 6350 50  0001 C CNN "LinkFournisseur"
F 10 "0,005" H 6050 6350 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6050 6350 50  0001 C CNN "DeviceFournisseur"
	1    6050 6350
	1    0    0    1   
$EndComp
$Comp
L Device:D_Small D4
U 1 1 5E5774AD
P 6200 6250
F 0 "D4" H 6100 6200 50  0000 C CNN
F 1 "D_Small" H 6450 6350 50  0001 R CNN
F 2 "" V 6200 6250 50  0001 C CNN
F 3 "~" V 6200 6250 50  0001 C CNN
	1    6200 6250
	1    0    0    1   
$EndComp
$Comp
L sifflet:TPS22918TDBVTQ1 U2
U 1 1 5E430158
P 5600 6400
F 0 "U2" H 5600 6650 50  0000 C CNN
F 1 "TPS22918TDBVTQ1" H 5600 6050 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6_Handsoldering" H 7100 6750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tps22918-q1.pdf" H 5600 6650 50  0001 C CNN
F 4 "TEXAS INSTRUMENTS" H 6200 6650 50  0001 L CNN "Fabricant"
F 5 "TPS22918-Q1" H 6200 6550 50  0001 L CNN "RefFabricant"
F 6 "http://www.ti.com/product/TPS22918-Q1" H 6200 6450 50  0001 L CNN "LinkFabricant"
F 7 "Farnell" H 6200 6350 50  0001 L CNN "Fournisseur"
F 8 "3008904" H 6200 6250 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=3008904" H 6200 6150 50  0001 L CNN "LinkFournisseur"
F 10 "0,531" H 6200 6050 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 6200 5950 50  0001 L CNN "DeviceFournisseur"
	1    5600 6400
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small Rl1
U 1 1 5E498719
P 4950 6350
F 0 "Rl1" H 4900 6500 50  0000 L BNN
F 1 "10k" V 4950 6350 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4950 6350 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 4950 6350 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 4950 6350 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 4950 6350 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 4950 6350 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 4950 6350 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 4950 6350 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 4950 6350 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 4950 6350 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 4950 6350 50  0001 C CNN "DeviceFournisseur"
	1    4950 6350
	1    0    0    1   
$EndComp
$Comp
L Device:R_Small Rext1
U 1 1 5E518732
P 5150 6350
F 0 "Rext1" H 5200 6150 50  0000 C BNN
F 1 "1k" V 5150 6350 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5150 6350 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 5150 6350 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 5150 6350 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 5150 6350 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 5150 6350 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5150 6350 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 5150 6350 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 5150 6350 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 5150 6350 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5150 6350 50  0001 C CNN "DeviceFournisseur"
	1    5150 6350
	1    0    0    -1  
$EndComp
Text Label 5950 4100 3    50   ~ 0
GPS_TRX1
Text Label 5850 4100 3    50   ~ 0
GPS_RTX1
Text Label 6250 4100 3    50   ~ 0
GPS_STANDBY
Text Label 6150 4100 3    50   ~ 0
GPS_SS
Wire Wire Line
	5850 4050 5850 4100
Wire Wire Line
	5950 4050 5950 4100
Wire Wire Line
	6150 4050 6150 4100
Wire Wire Line
	6250 4050 6250 4100
$Comp
L Device:Antenna AE2
U 1 1 5E8CB784
P 7900 2750
F 0 "AE2" H 7980 2739 50  0000 L CNN
F 1 "Active Antenna" H 7980 2648 50  0000 L CNN
F 2 "" H 7900 2750 50  0001 C CNN
F 3 "~" H 7900 2750 50  0001 C CNN
	1    7900 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5E2A66BE
P 7600 3300
F 0 "#PWR0117" H 7600 3050 50  0001 C CNN
F 1 "GND" H 7605 3127 50  0001 C CNN
F 2 "" H 7600 3300 50  0001 C CNN
F 3 "" H 7600 3300 50  0001 C CNN
	1    7600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 3300 7600 3250
$Comp
L power:GND #PWR0118
U 1 1 5E2A66C5
P 7900 3300
F 0 "#PWR0118" H 7900 3050 50  0001 C CNN
F 1 "GND" H 7905 3127 50  0001 C CNN
F 2 "" H 7900 3300 50  0001 C CNN
F 3 "" H 7900 3300 50  0001 C CNN
	1    7900 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3300 7900 3250
$Comp
L Device:C_Small C12
U 1 1 5E2A66CC
P 7900 3150
F 0 "C12" H 8000 3150 50  0000 L CNN
F 1 "NM" H 8000 3050 50  0000 L CNN
F 2 "" H 7900 3150 50  0001 C CNN
F 3 "~" H 7900 3150 50  0001 C CNN
	1    7900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5E2A66D2
P 7600 3150
F 0 "C11" H 7700 3150 50  0000 L CNN
F 1 "NM" H 7700 3050 50  0000 L CNN
F 2 "" H 7600 3150 50  0001 C CNN
F 3 "~" H 7600 3150 50  0001 C CNN
	1    7600 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3000 7850 3000
Wire Wire Line
	7300 3000 7600 3000
$Comp
L Device:L_Small L2
U 1 1 5E2C4E89
P 7750 3000
F 0 "L2" V 7843 3000 50  0000 C CNN
F 1 "L_Small" V 7844 3000 50  0001 C CNN
F 2 "" H 7750 3000 50  0001 C CNN
F 3 "~" H 7750 3000 50  0001 C CNN
	1    7750 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 2950 7900 3000
Connection ~ 7900 3000
Wire Wire Line
	7900 3000 7900 3050
Wire Wire Line
	7600 3050 7600 3000
Connection ~ 7600 3000
Wire Wire Line
	7600 3000 7650 3000
$Comp
L Switch:SW_Push SW1
U 1 1 5E2E11AF
P 8000 2000
F 0 "SW1" H 8000 2285 50  0000 C CNN
F 1 "SW_Push" H 8000 2194 50  0000 C CNN
F 2 "" H 8000 2200 50  0001 C CNN
F 3 "~" H 8000 2200 50  0001 C CNN
	1    8000 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E2E30D4
P 8200 2250
F 0 "#PWR0119" H 8200 2000 50  0001 C CNN
F 1 "GND" H 8205 2077 50  0001 C CNN
F 2 "" H 8200 2250 50  0001 C CNN
F 3 "" H 8200 2250 50  0001 C CNN
	1    8200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 2250 8200 2200
Wire Wire Line
	8100 2200 8200 2200
Wire Wire Line
	8200 2200 8200 2000
Connection ~ 8200 2200
Wire Wire Line
	7800 2000 7800 2200
Wire Wire Line
	7800 2200 7900 2200
Connection ~ 7800 2200
$Comp
L Device:C_Small C13
U 1 1 5E30BB21
P 8000 2200
F 0 "C13" V 7950 2300 50  0000 C CNN
F 1 "0.1uF" V 8050 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8000 2200 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 8000 2200 50  0001 C CNN
F 4 "muRata" H 8000 2200 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 8000 2200 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 8000 2200 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 8000 2200 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 8000 2200 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 8000 2200 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 8000 2200 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 8000 2200 50  0001 C CNN "DeviceFournisseur"
	1    8000 2200
	0    -1   1    0   
$EndComp
Wire Wire Line
	5550 1050 5550 800 
Wire Wire Line
	5550 800  5650 800 
Wire Wire Line
	5650 800  5650 1050
Text Label 3950 2250 2    50   ~ 0
VDD_TCXO
Text Label 7300 2200 0    50   ~ 0
LORA_RESET
Wire Wire Line
	7300 2200 7800 2200
Text Label 6050 800  0    50   ~ 0
LORA_SWCLK
Text Label 6150 900  0    50   ~ 0
LORA_SWDIO
Wire Wire Line
	6150 900  6150 1050
Wire Wire Line
	6050 800  6050 1050
Text Label 2450 5950 0    50   ~ 0
GPS_TIMER
Text Label 2450 5750 0    50   ~ 0
GPS_RESET
Text Label 2450 6050 0    50   ~ 0
GPS_1PPS
Text Label 2450 5850 0    50   ~ 0
GPS_STANDBY
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5E3FEE91
P 2250 5950
F 0 "J1" H 2330 5942 50  0000 L CNN
F 1 "Conn_01x04" H 2330 5851 50  0000 L CNN
F 2 "" H 2250 5950 50  0001 C CNN
F 3 "~" H 2250 5950 50  0001 C CNN
	1    2250 5950
	-1   0    0    1   
$EndComp
Connection ~ 9450 4000
Wire Wire Line
	9450 4050 9450 4000
$Comp
L power:GND #PWR0120
U 1 1 5E43CAEF
P 9450 4050
F 0 "#PWR0120" H 9450 3800 50  0001 C CNN
F 1 "GND" H 9455 3877 50  0001 C CNN
F 2 "" H 9450 4050 50  0001 C CNN
F 3 "" H 9450 4050 50  0001 C CNN
	1    9450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 4000 9450 3950
Wire Wire Line
	9350 4000 9450 4000
Wire Wire Line
	9350 3950 9350 4000
Text Label 9450 2750 0    50   ~ 0
LORA_VDD
Text Label 9950 3350 0    50   ~ 0
LORA_SWDIO
Text Label 9950 3250 0    50   ~ 0
LORA_SWCLK
Text Label 9950 3050 0    50   ~ 0
LORA_RESET
$Comp
L Connector:Conn_ARM_JTAG_SWD_10 J3
U 1 1 5E3BF3E3
P 9450 3350
F 0 "J3" H 9900 3000 50  0000 L CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 9900 2900 50  0000 L CNN
F 2 "" H 9450 3350 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 9100 2100 50  0001 C CNN
	1    9450 3350
	1    0    0    -1  
$EndComp
Text Label 3950 2550 2    50   ~ 0
LORA_VDD_USB
Text Label 3950 2750 2    50   ~ 0
LORA_VDD_RF
Wire Wire Line
	4200 2550 3950 2550
Wire Wire Line
	4150 2750 3950 2750
Connection ~ 4150 2750
Text Label 5450 4900 2    50   ~ 0
LORA_VDD_USB
Text Label 6250 4900 0    50   ~ 0
LORA_VDD_RF
Wire Wire Line
	6250 4900 6100 4900
Connection ~ 6100 4900
Connection ~ 5600 4900
Wire Wire Line
	5600 4800 5600 4900
Wire Wire Line
	5800 4900 5800 4800
Connection ~ 5800 4900
$Comp
L Device:C_Small C7
U 1 1 5E17BB44
P 5800 5000
F 0 "C7" H 5850 5150 50  0000 R BNN
F 1 "10uF" H 5800 5250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5800 5000 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R60J106KE47-01A.pdf" H 5800 5000 50  0001 C CNN
F 4 "muRata" H 5800 5000 50  0001 C CNN "Fabricant"
F 5 "GRM188R60J106KE47D" H 5800 5000 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R60J106KE47%23.html" H 5800 5000 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5800 5000 50  0001 C CNN "Fournisseur"
F 8 "2494230" H 5800 5000 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2494230" H 5800 5000 50  0001 C CNN "LinkFournisseur"
F 10 "0,148" H 5800 5000 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5800 5000 50  0001 C CNN "DeviceFournisseur"
	1    5800 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 2250 4200 2250
$Comp
L sifflet:SAMSUNG-SSD J2
U 1 1 5E2F4744
P 9450 2050
F 0 "J2" H 9200 2300 50  0000 R CNN
F 1 "SAMSUNG-SSD" H 9200 2200 50  0000 R CNN
F 2 "sifflet:SAMSUNG_SSD1" H 9800 2450 50  0001 L CNN
F 3 "https://www.samsung.com/semiconductor/global.semi.static/Samsung_SSD_860_EVO_Data_Sheet_Rev1.pdf" H 9450 2000 50  0001 L CNN
F 4 "Samsung" H 9800 2350 50  0001 L CNN "Fabricant"
F 5 "mz-76e250beu" H 9800 2250 50  0001 L CNN "RefFabricant"
F 6 "https://www.samsung.com/fr/business/memory-storage/860-evo-3-ssd/mz-76e250beu/" H 9800 2150 50  0001 L CNN "LinkFabricant"
F 7 "AliExpress" H 9800 2050 50  0001 L CNN "Fournisseur"
F 8 "32852808447" H 9800 1950 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/32852808447.html" H 9800 1850 50  0001 L CNN "LinkFournisseur"
F 10 "51,46" H 9800 1750 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 9800 1650 50  0001 L CNN "DeviceFournisseur"
	1    9450 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9450 2500 9450 2450
$Comp
L power:GND #PWR0121
U 1 1 5E31B0E6
P 9450 2500
F 0 "#PWR0121" H 9450 2250 50  0001 C CNN
F 1 "GND" H 9455 2327 50  0001 C CNN
F 2 "" H 9450 2500 50  0001 C CNN
F 3 "" H 9450 2500 50  0001 C CNN
	1    9450 2500
	1    0    0    -1  
$EndComp
Text Label 9600 1550 0    50   ~ 0
+1V82
Text Label 9500 1450 0    50   ~ 0
+1V84
Text Label 9400 1350 0    50   ~ 0
+2V96
Text Label 9300 1250 0    50   ~ 0
+12V3
Wire Wire Line
	9600 1600 9600 1550
Wire Wire Line
	9500 1450 9500 1600
Wire Wire Line
	9400 1350 9400 1600
Wire Wire Line
	9300 1250 9300 1600
$EndSCHEMATC
