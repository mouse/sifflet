EESchema Schematic File Version 4
LIBS:sifflet-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5150 1450 5150 1400
Wire Wire Line
	5150 1400 5050 1400
Wire Wire Line
	5050 1400 5050 1450
Wire Wire Line
	5050 1400 4950 1400
Wire Wire Line
	4950 1400 4950 1450
Connection ~ 5050 1400
Wire Wire Line
	4950 1400 4850 1400
Wire Wire Line
	4850 1400 4850 1450
Connection ~ 4950 1400
Wire Wire Line
	4850 1400 4750 1400
Wire Wire Line
	4750 1400 4750 1450
Connection ~ 4850 1400
Wire Wire Line
	4750 1400 4350 1400
Wire Wire Line
	4350 1400 4350 1850
Wire Wire Line
	4350 1850 4400 1850
Connection ~ 4750 1400
Wire Wire Line
	4350 1850 4350 1950
Wire Wire Line
	4350 1950 4400 1950
Connection ~ 4350 1850
Wire Wire Line
	4350 1950 4350 2050
Wire Wire Line
	4350 2050 4400 2050
Connection ~ 4350 1950
Wire Wire Line
	4350 2050 4350 2150
Wire Wire Line
	4350 2150 4400 2150
Connection ~ 4350 2050
$Comp
L power:GND #PWR0101
U 1 1 5E176BDA
P 4350 2150
F 0 "#PWR0101" H 4350 1900 50  0001 C CNN
F 1 "GND" H 4355 1977 50  0001 C CNN
F 2 "" H 4350 2150 50  0001 C CNN
F 3 "" H 4350 2150 50  0001 C CNN
	1    4350 2150
	1    0    0    -1  
$EndComp
Connection ~ 4350 2150
Wire Wire Line
	4400 3250 4350 3250
$Comp
L Device:C_Small C8
U 1 1 5E17B28C
P 6150 5400
F 0 "C8" H 6200 5650 50  0000 R BNN
F 1 "1uF" H 6200 5750 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6150 5400 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 6150 5400 50  0001 C CNN
F 4 "muRata" H 6150 5400 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 6150 5400 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 6150 5400 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6150 5400 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 6150 5400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 6150 5400 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 6150 5400 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6150 5400 50  0001 C CNN "DeviceFournisseur"
	1    6150 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5E17CC27
P 6300 5400
F 0 "C9" H 6300 5550 50  0000 R BNN
F 1 "0.1uF" H 6250 5650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6300 5400 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 6300 5400 50  0001 C CNN
F 4 "muRata" H 6300 5400 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 6300 5400 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 6300 5400 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6300 5400 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 6300 5400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 6300 5400 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 6300 5400 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6300 5400 50  0001 C CNN "DeviceFournisseur"
	1    6300 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5E17E7E2
P 5650 5400
F 0 "C5" H 5700 5550 50  0000 R BNN
F 1 "10uF" H 5750 5650 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5650 5400 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R60J106KE47-01A.pdf" H 5650 5400 50  0001 C CNN
F 4 "muRata" H 5650 5400 50  0001 C CNN "Fabricant"
F 5 "GRM188R60J106KE47D" H 5650 5400 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R60J106KE47%23.html" H 5650 5400 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5650 5400 50  0001 C CNN "Fournisseur"
F 8 "2494230" H 5650 5400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2494230" H 5650 5400 50  0001 C CNN "LinkFournisseur"
F 10 "0,148" H 5650 5400 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5650 5400 50  0001 C CNN "DeviceFournisseur"
	1    5650 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5E17F1DB
P 5800 5400
F 0 "C6" H 5850 5650 50  0000 R BNN
F 1 "0.1uF" H 5800 5750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5800 5400 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 5800 5400 50  0001 C CNN
F 4 "muRata" H 5800 5400 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 5800 5400 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 5800 5400 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 5800 5400 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 5800 5400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 5800 5400 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 5800 5400 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 5800 5400 50  0001 C CNN "DeviceFournisseur"
	1    5800 5400
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5E1829A7
P 6150 1300
F 0 "R6" H 6100 1150 50  0000 L CNN
F 1 "10k" V 6150 1300 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6150 1300 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 6150 1300 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 6150 1300 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 6150 1300 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 6150 1300 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6150 1300 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 6150 1300 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 6150 1300 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 6150 1300 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6150 1300 50  0001 C CNN "DeviceFournisseur"
	1    6150 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 5500 5800 5500
Wire Wire Line
	4400 3050 4350 3050
Wire Wire Line
	4350 3050 4350 3150
Wire Wire Line
	4350 3150 4400 3150
Wire Wire Line
	5650 5300 5800 5300
Wire Wire Line
	6000 5300 6150 5300
Connection ~ 6150 5300
Wire Wire Line
	6150 5300 6300 5300
Wire Wire Line
	6300 5500 6150 5500
Connection ~ 5800 5500
Connection ~ 6000 5500
Wire Wire Line
	6000 5500 5800 5500
Connection ~ 6150 5500
Wire Wire Line
	6150 5500 6000 5500
Wire Wire Line
	6300 5500 6450 5500
Connection ~ 6300 5500
$Comp
L power:GND #PWR0102
U 1 1 5E1970B7
P 6450 5500
F 0 "#PWR0102" H 6450 5250 50  0001 C CNN
F 1 "GND" H 6455 5327 50  0001 C CNN
F 2 "" H 6450 5500 50  0001 C CNN
F 3 "" H 6450 5500 50  0001 C CNN
	1    6450 5500
	1    0    0    -1  
$EndComp
Text Label 6000 5200 0    50   ~ 0
VDD
Wire Wire Line
	4400 2850 4350 2850
$Comp
L sifflet:CMWX1ZZABZ-091 U3
U 1 1 5E16F702
P 5950 2950
F 0 "U3" H 7450 4300 50  0000 L CNN
F 1 "CMWX1ZZABZ-091" H 7450 4200 50  0000 L CNN
F 2 "sifflet:MODULE_CMWX1ZZABZ-091" H 5950 4350 50  0001 C CNN
F 3 "https://wireless.murata.com/pub/RFM/data/type_abz.pdf" H 5950 4350 50  0001 L CNN
F 4 "muRata" H 5150 950 50  0001 L CNN "Fabricant"
F 5 "CMWX1ZZABZ-091" H 5150 850 50  0001 L CNN "RefFabricant"
F 6 "https://wireless.murata.com/type-abz.html" H 5150 750 50  0001 L CNN "LinkFabricant"
F 7 "Farnell" H 5150 650 50  0001 L CNN "Fournisseur"
F 8 "2802546" H 5150 550 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2802546" H 5150 450 50  0001 L CNN "LinkFournisseur"
F 10 "16,72" H 5150 350 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 5150 250 50  0001 L CNN "DeviceFournisseur"
	1    5950 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E177ED9
P 4350 2850
F 0 "#PWR0103" H 4350 2600 50  0001 C CNN
F 1 "GND" H 4355 2677 50  0001 C CNN
F 2 "" H 4350 2850 50  0001 C CNN
F 3 "" H 4350 2850 50  0001 C CNN
	1    4350 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5E17832A
P 4350 3250
F 0 "#PWR0104" H 4350 3000 50  0001 C CNN
F 1 "GND" H 4355 3077 50  0001 C CNN
F 2 "" H 4350 3250 50  0001 C CNN
F 3 "" H 4350 3250 50  0001 C CNN
	1    4350 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E19FE69
P 5950 1200
F 0 "#PWR0105" H 5950 950 50  0001 C CNN
F 1 "GND" H 5955 1027 50  0001 C CNN
F 2 "" H 5950 1200 50  0001 C CNN
F 3 "" H 5950 1200 50  0001 C CNN
	1    5950 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1200 6050 1200
Wire Wire Line
	6050 1200 6050 1450
Connection ~ 6050 1200
Wire Wire Line
	6050 1200 5950 1200
Wire Wire Line
	6150 1400 6150 1450
Wire Wire Line
	7500 3300 7550 3300
Wire Wire Line
	7550 3500 7500 3500
Text Label 5650 1300 2    50   ~ 0
VDD_TCXO
Text Label 5800 5200 2    50   ~ 0
VDD_USD
$Comp
L power:GND #PWR0106
U 1 1 5E1B3383
P 7550 3500
F 0 "#PWR0106" H 7550 3250 50  0001 C CNN
F 1 "GND" H 7555 3327 50  0001 C CNN
F 2 "" H 7550 3500 50  0001 C CNN
F 3 "" H 7550 3500 50  0001 C CNN
	1    7550 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5E1B3CF8
P 7550 3300
F 0 "#PWR0107" H 7550 3050 50  0001 C CNN
F 1 "GND" H 7555 3127 50  0001 C CNN
F 2 "" H 7550 3300 50  0001 C CNN
F 3 "" H 7550 3300 50  0001 C CNN
	1    7550 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5E1C0070
P 7800 2900
F 0 "C10" H 8050 2850 50  0000 R CNN
F 1 "1uF" H 8050 2900 50  0000 R BNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7800 2900 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GCM188R71C105KA64-01.pdf" H 7800 2900 50  0001 C CNN
F 4 "muRata" H 7800 2900 50  0001 C CNN "Fabricant"
F 5 "GCM188R71C105KA64D" H 7800 2900 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GCM188R71C105KA64%23.html" H 7800 2900 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 7800 2900 50  0001 C CNN "Fournisseur"
F 8 "2470423" H 7800 2900 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2470423" H 7800 2900 50  0001 C CNN "LinkFournisseur"
F 10 "0,11" H 7800 2900 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 7800 2900 50  0001 C CNN "DeviceFournisseur"
	1    7800 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 2800 7800 2800
Text Label 7800 2800 0    50   ~ 0
VREF+
Text Label 8100 2800 0    50   ~ 0
LORA_VDD
Wire Wire Line
	7800 3000 7800 3050
Wire Wire Line
	8100 2800 7800 2800
Connection ~ 7800 2800
$Comp
L power:GND #PWR0108
U 1 1 5E1C4525
P 7800 3050
F 0 "#PWR0108" H 7800 2800 50  0001 C CNN
F 1 "GND" H 7805 2877 50  0001 C CNN
F 2 "" H 7800 3050 50  0001 C CNN
F 3 "" H 7800 3050 50  0001 C CNN
	1    7800 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1300 5650 1450
Text Notes 4500 6250 0    50   ~ 0
https://hackaday.io/project/35169-hackable-cmwx1zzabz-lora-devices\nhttps://wireless.murata.com/pub/RFM/data/type_abz.pdf#page=9\n
Text Label 6150 4500 3    50   ~ 0
GPS_TRX1
Text Label 6050 4500 3    50   ~ 0
GPS_RTX1
Text Label 6450 4500 3    50   ~ 0
GPS_STANDBY
Text Label 6350 4500 3    50   ~ 0
GPS_SS
Wire Wire Line
	6050 4450 6050 4500
Wire Wire Line
	6150 4450 6150 4500
Wire Wire Line
	6350 4450 6350 4500
Wire Wire Line
	6450 4450 6450 4500
$Comp
L Device:Antenna AE2
U 1 1 5E8CB784
P 8100 3150
F 0 "AE2" H 8180 3139 50  0000 L CNN
F 1 "Active Antenna" H 8180 3048 50  0000 L CNN
F 2 "" H 8100 3150 50  0001 C CNN
F 3 "~" H 8100 3150 50  0001 C CNN
	1    8100 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5E2A66BE
P 7800 3700
F 0 "#PWR0117" H 7800 3450 50  0001 C CNN
F 1 "GND" H 7805 3527 50  0001 C CNN
F 2 "" H 7800 3700 50  0001 C CNN
F 3 "" H 7800 3700 50  0001 C CNN
	1    7800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3700 7800 3650
$Comp
L power:GND #PWR0118
U 1 1 5E2A66C5
P 8100 3700
F 0 "#PWR0118" H 8100 3450 50  0001 C CNN
F 1 "GND" H 8105 3527 50  0001 C CNN
F 2 "" H 8100 3700 50  0001 C CNN
F 3 "" H 8100 3700 50  0001 C CNN
	1    8100 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3700 8100 3650
$Comp
L Device:C_Small C12
U 1 1 5E2A66CC
P 8100 3550
F 0 "C12" H 8200 3550 50  0000 L CNN
F 1 "NM" H 8200 3450 50  0000 L CNN
F 2 "" H 8100 3550 50  0001 C CNN
F 3 "~" H 8100 3550 50  0001 C CNN
	1    8100 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 5E2A66D2
P 7800 3550
F 0 "C11" H 7900 3550 50  0000 L CNN
F 1 "NM" H 7900 3450 50  0000 L CNN
F 2 "" H 7800 3550 50  0001 C CNN
F 3 "~" H 7800 3550 50  0001 C CNN
	1    7800 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3400 8050 3400
Wire Wire Line
	7500 3400 7800 3400
$Comp
L Device:L_Small L2
U 1 1 5E2C4E89
P 7950 3400
F 0 "L2" V 8043 3400 50  0000 C CNN
F 1 "L_Small" V 8044 3400 50  0001 C CNN
F 2 "" H 7950 3400 50  0001 C CNN
F 3 "~" H 7950 3400 50  0001 C CNN
	1    7950 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8100 3350 8100 3400
Connection ~ 8100 3400
Wire Wire Line
	8100 3400 8100 3450
Wire Wire Line
	7800 3450 7800 3400
Connection ~ 7800 3400
Wire Wire Line
	7800 3400 7850 3400
$Comp
L Switch:SW_Push SW1
U 1 1 5E2E11AF
P 8200 2400
F 0 "SW1" H 8200 2685 50  0000 C CNN
F 1 "SW_Push" H 8200 2594 50  0000 C CNN
F 2 "" H 8200 2600 50  0001 C CNN
F 3 "~" H 8200 2600 50  0001 C CNN
	1    8200 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E2E30D4
P 8400 2650
F 0 "#PWR0119" H 8400 2400 50  0001 C CNN
F 1 "GND" H 8405 2477 50  0001 C CNN
F 2 "" H 8400 2650 50  0001 C CNN
F 3 "" H 8400 2650 50  0001 C CNN
	1    8400 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2650 8400 2600
Wire Wire Line
	8300 2600 8400 2600
Wire Wire Line
	8400 2600 8400 2400
Connection ~ 8400 2600
Wire Wire Line
	8000 2400 8000 2600
Wire Wire Line
	8000 2600 8100 2600
Connection ~ 8000 2600
$Comp
L Device:C_Small C13
U 1 1 5E30BB21
P 8200 2600
F 0 "C13" V 8150 2700 50  0000 C CNN
F 1 "0.1uF" V 8250 2750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8200 2600 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 8200 2600 50  0001 C CNN
F 4 "muRata" H 8200 2600 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 8200 2600 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 8200 2600 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 8200 2600 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 8200 2600 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 8200 2600 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 8200 2600 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 8200 2600 50  0001 C CNN "DeviceFournisseur"
	1    8200 2600
	0    -1   1    0   
$EndComp
Wire Wire Line
	5750 1450 5750 1200
Wire Wire Line
	5750 1200 5850 1200
Wire Wire Line
	5850 1200 5850 1450
Text Label 4150 2650 2    50   ~ 0
VDD_TCXO
Text Label 7500 2600 0    50   ~ 0
LORA_RESET
Wire Wire Line
	7500 2600 8000 2600
Text Label 6250 1200 0    50   ~ 0
LORA_SWCLK
Text Label 6350 1300 0    50   ~ 0
LORA_SWDIO
Wire Wire Line
	6350 1300 6350 1450
Wire Wire Line
	6250 1200 6250 1450
Connection ~ 9650 4400
Wire Wire Line
	9650 4450 9650 4400
$Comp
L power:GND #PWR0120
U 1 1 5E43CAEF
P 9650 4450
F 0 "#PWR0120" H 9650 4200 50  0001 C CNN
F 1 "GND" H 9655 4277 50  0001 C CNN
F 2 "" H 9650 4450 50  0001 C CNN
F 3 "" H 9650 4450 50  0001 C CNN
	1    9650 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4400 9650 4350
Wire Wire Line
	9550 4400 9650 4400
Wire Wire Line
	9550 4350 9550 4400
Text Label 9650 3150 0    50   ~ 0
LORA_VDD
Text Label 10150 3750 0    50   ~ 0
LORA_SWDIO
Text Label 10150 3650 0    50   ~ 0
LORA_SWCLK
Text Label 10150 3450 0    50   ~ 0
LORA_RESET
$Comp
L Connector:Conn_ARM_JTAG_SWD_10 J3
U 1 1 5E3BF3E3
P 9650 3750
F 0 "J3" H 10100 3400 50  0000 L CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 10100 3300 50  0000 L CNN
F 2 "" H 9650 3750 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 9300 2500 50  0001 C CNN
	1    9650 3750
	1    0    0    -1  
$EndComp
Text Label 4150 2950 2    50   ~ 0
LORA_VDD_USB
Text Label 4150 3150 2    50   ~ 0
LORA_VDD_RF
Wire Wire Line
	4400 2950 4150 2950
Wire Wire Line
	4350 3150 4150 3150
Connection ~ 4350 3150
Text Label 5650 5300 2    50   ~ 0
LORA_VDD_USB
Text Label 6450 5300 0    50   ~ 0
LORA_VDD_RF
Wire Wire Line
	6450 5300 6300 5300
Connection ~ 6300 5300
Connection ~ 5800 5300
Wire Wire Line
	5800 5200 5800 5300
Wire Wire Line
	6000 5300 6000 5200
Connection ~ 6000 5300
$Comp
L Device:C_Small C7
U 1 1 5E17BB44
P 6000 5400
F 0 "C7" H 6050 5550 50  0000 R BNN
F 1 "10uF" H 6000 5650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6000 5400 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R60J106KE47-01A.pdf" H 6000 5400 50  0001 C CNN
F 4 "muRata" H 6000 5400 50  0001 C CNN "Fabricant"
F 5 "GRM188R60J106KE47D" H 6000 5400 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R60J106KE47%23.html" H 6000 5400 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 6000 5400 50  0001 C CNN "Fournisseur"
F 8 "2494230" H 6000 5400 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2494230" H 6000 5400 50  0001 C CNN "LinkFournisseur"
F 10 "0,148" H 6000 5400 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 6000 5400 50  0001 C CNN "DeviceFournisseur"
	1    6000 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 2650 4400 2650
$Comp
L sifflet:SAMSUNG-SSD J2
U 1 1 5E2F4744
P 9650 2450
F 0 "J2" H 9400 2700 50  0000 R CNN
F 1 "SAMSUNG-SSD" H 9400 2600 50  0000 R CNN
F 2 "sifflet:SAMSUNG_SSD1" H 10000 2850 50  0001 L CNN
F 3 "https://www.samsung.com/semiconductor/global.semi.static/Samsung_SSD_860_EVO_Data_Sheet_Rev1.pdf" H 9650 2400 50  0001 L CNN
F 4 "Samsung" H 10000 2750 50  0001 L CNN "Fabricant"
F 5 "mz-76e250beu" H 10000 2650 50  0001 L CNN "RefFabricant"
F 6 "https://www.samsung.com/fr/business/memory-storage/860-evo-3-ssd/mz-76e250beu/" H 10000 2550 50  0001 L CNN "LinkFabricant"
F 7 "AliExpress" H 10000 2450 50  0001 L CNN "Fournisseur"
F 8 "32852808447" H 10000 2350 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.aliexpress.com/item/32852808447.html" H 10000 2250 50  0001 L CNN "LinkFournisseur"
F 10 "51,46" H 10000 2150 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 10000 2050 50  0001 L CNN "DeviceFournisseur"
	1    9650 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9650 2900 9650 2850
$Comp
L power:GND #PWR0121
U 1 1 5E31B0E6
P 9650 2900
F 0 "#PWR0121" H 9650 2650 50  0001 C CNN
F 1 "GND" H 9655 2727 50  0001 C CNN
F 2 "" H 9650 2900 50  0001 C CNN
F 3 "" H 9650 2900 50  0001 C CNN
	1    9650 2900
	1    0    0    -1  
$EndComp
Text Label 9800 1950 0    50   ~ 0
+1V82
Text Label 9700 1850 0    50   ~ 0
+1V84
Text Label 9600 1750 0    50   ~ 0
+2V96
Text Label 9500 1650 0    50   ~ 0
+12V3
Wire Wire Line
	9800 2000 9800 1950
Wire Wire Line
	9700 1850 9700 2000
Wire Wire Line
	9600 1750 9600 2000
Wire Wire Line
	9500 1650 9500 2000
$Comp
L sifflet:AG9 U1
U 1 1 5E339197
P 1250 2100
F 0 "U1" H 1308 3015 50  0000 C CNN
F 1 "AG9" H 1308 2924 50  0000 C CNN
F 2 "sifflet:A9G" H 900 2750 50  0001 R CNN
F 3 "http://wiki.ai-thinker.com/_media/a6_a9_a9g_gprs_user_manual.pdf" H 1600 1700 50  0001 R CNN
F 4 "Ai-Thinker" H 900 2650 50  0001 R CNN "Fabricant"
F 5 "AG9" H 900 2550 50  0001 R CNN "RefFabricant"
F 6 "http://wiki.ai-thinker.com/_media/b102ps01a2_a9g_product_specification.pdf" H 900 2450 50  0001 R CNN "LinkFabricant"
F 7 "AliExpress" H 900 2350 50  0001 R CNN "Fournisseur"
F 8 "32961147496" H 900 2250 50  0001 R CNN "RefFournisseur"
F 9 "https://www.aliexpress.com/item/32961147496.html" H 900 2150 50  0001 R CNN "LinkFournisseur"
F 10 "3,59" H 900 2050 50  0001 R CNN "CostFournisseur"
F 11 "EUR" H 900 1950 50  0001 R CNN "DeviceFournisseur"
	1    1250 2100
	1    0    0    -1  
$EndComp
$Comp
L sifflet:AG9 U1
U 3 1 5E349B53
P 2300 2100
F 0 "U1" H 2358 3015 50  0000 C CNN
F 1 "AG9" H 2358 2924 50  0000 C CNN
F 2 "sifflet:A9G" H 1950 2750 50  0001 R CNN
F 3 "http://wiki.ai-thinker.com/_media/a6_a9_a9g_gprs_user_manual.pdf" H 2650 1700 50  0001 R CNN
F 4 "Ai-Thinker" H 1950 2650 50  0001 R CNN "Fabricant"
F 5 "AG9" H 1950 2550 50  0001 R CNN "RefFabricant"
F 6 "http://wiki.ai-thinker.com/_media/b102ps01a2_a9g_product_specification.pdf" H 1950 2450 50  0001 R CNN "LinkFabricant"
F 7 "AliExpress" H 1950 2350 50  0001 R CNN "Fournisseur"
F 8 "32961147496" H 1950 2250 50  0001 R CNN "RefFournisseur"
F 9 "https://www.aliexpress.com/item/32961147496.html" H 1950 2150 50  0001 R CNN "LinkFournisseur"
F 10 "3,59" H 1950 2050 50  0001 R CNN "CostFournisseur"
F 11 "EUR" H 1950 1950 50  0001 R CNN "DeviceFournisseur"
	3    2300 2100
	1    0    0    -1  
$EndComp
$Comp
L sifflet:AG9 U1
U 2 1 5E34C6F7
P 1250 3950
F 0 "U1" H 1308 4865 50  0000 C CNN
F 1 "AG9" H 1308 4774 50  0000 C CNN
F 2 "sifflet:A9G" H 900 4600 50  0001 R CNN
F 3 "http://wiki.ai-thinker.com/_media/a6_a9_a9g_gprs_user_manual.pdf" H 1600 3550 50  0001 R CNN
F 4 "Ai-Thinker" H 900 4500 50  0001 R CNN "Fabricant"
F 5 "AG9" H 900 4400 50  0001 R CNN "RefFabricant"
F 6 "http://wiki.ai-thinker.com/_media/b102ps01a2_a9g_product_specification.pdf" H 900 4300 50  0001 R CNN "LinkFabricant"
F 7 "AliExpress" H 900 4200 50  0001 R CNN "Fournisseur"
F 8 "32961147496" H 900 4100 50  0001 R CNN "RefFournisseur"
F 9 "https://www.aliexpress.com/item/32961147496.html" H 900 4000 50  0001 R CNN "LinkFournisseur"
F 10 "3,59" H 900 3900 50  0001 R CNN "CostFournisseur"
F 11 "EUR" H 900 3800 50  0001 R CNN "DeviceFournisseur"
	2    1250 3950
	1    0    0    -1  
$EndComp
$Comp
L sifflet:AG9 U1
U 4 1 5E34FA59
P 2300 3950
F 0 "U1" H 2358 4865 50  0000 C CNN
F 1 "AG9" H 2358 4774 50  0000 C CNN
F 2 "sifflet:A9G" H 1950 4600 50  0001 R CNN
F 3 "http://wiki.ai-thinker.com/_media/a6_a9_a9g_gprs_user_manual.pdf" H 2650 3550 50  0001 R CNN
F 4 "Ai-Thinker" H 1950 4500 50  0001 R CNN "Fabricant"
F 5 "AG9" H 1950 4400 50  0001 R CNN "RefFabricant"
F 6 "http://wiki.ai-thinker.com/_media/b102ps01a2_a9g_product_specification.pdf" H 1950 4300 50  0001 R CNN "LinkFabricant"
F 7 "AliExpress" H 1950 4200 50  0001 R CNN "Fournisseur"
F 8 "32961147496" H 1950 4100 50  0001 R CNN "RefFournisseur"
F 9 "https://www.aliexpress.com/item/32961147496.html" H 1950 4000 50  0001 R CNN "LinkFournisseur"
F 10 "3,59" H 1950 3900 50  0001 R CNN "CostFournisseur"
F 11 "EUR" H 1950 3800 50  0001 R CNN "DeviceFournisseur"
	4    2300 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:SIM_Card J?
U 1 1 5E398AA7
P 9600 5450
F 0 "J?" H 10230 5550 50  0000 L CNN
F 1 "SIM_Card" H 10230 5459 50  0000 L CNN
F 2 "sifflet:2174803-2" H 9600 5800 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F2_1773464_0_SIM_QRG%7F0217%7Fpdf%7FEnglish%7FENG_DS_2_1773464_0_SIM_QRG_0217.pdf%7F2174803-2" H 9550 5450 50  0001 L CNN
F 4 "TE CONNECTIVITY" H 9600 5450 50  0001 L CNN "Fabricant"
F 5 "2174803-2" H 9600 5450 50  0001 L CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2174803-2.html" H 9600 5450 50  0001 L CNN "LinkFabricant"
F 7 "Farnell" H 9600 5450 50  0001 L CNN "Fournisseur"
F 8 "2778491" H 9600 5450 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2778491" H 9600 5450 50  0001 L CNN "LinkFournisseur"
F 10 "1,54" H 9600 5450 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 9600 5450 50  0001 L CNN "DeviceFournisseur"
	1    9600 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	9500 4950 9500 4900
NoConn ~ 9500 4900
NoConn ~ 8250 4700
$Comp
L Switch:SW_Push SW?
U 1 1 5E3AA900
P 1900 6700
F 0 "SW?" V 2050 6850 50  0000 C CNN
F 1 "SW_Push" H 1900 6894 50  0001 C CNN
F 2 "" H 1900 6900 50  0001 C CNN
F 3 "~" H 1900 6900 50  0001 C CNN
	1    1900 6700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E3ACE19
P 1750 6450
F 0 "R?" V 1700 6450 50  0000 C BNN
F 1 "10k" V 1750 6450 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1750 6450 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf" H 1750 6450 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 1750 6450 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J10K" H 1750 6450 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-2-2176340-5.html" H 1750 6450 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1750 6450 50  0001 C CNN "Fournisseur"
F 8 "2861870" H 1750 6450 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861870" H 1750 6450 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 1750 6450 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1750 6450 50  0001 C CNN "DeviceFournisseur"
	1    1750 6450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E3B2203
P 2200 6600
F 0 "C?" H 2100 6500 50  0000 R BNN
F 1 "100nF" H 2100 6600 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2200 6600 50  0001 C CNN
F 3 "https://search.murata.co.jp/Ceramy/image/img/A01X/G101/ENG/GRM188R72A104KA35-01A.pdf" H 2200 6600 50  0001 C CNN
F 4 "muRata" H 2200 6600 50  0001 C CNN "Fabricant"
F 5 "GRM188R72A104KA35D" H 2200 6600 50  0001 C CNN "RefFabricant"
F 6 "https://psearch.en.murata.com/capacitor/product/GRM188R72A104KA35%23.html" H 2200 6600 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 2200 6600 50  0001 C CNN "Fournisseur"
F 8 "1828921" H 2200 6600 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1828921" H 2200 6600 50  0001 C CNN "LinkFournisseur"
F 10 "0,219" H 2200 6600 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 2200 6600 50  0001 C CNN "DeviceFournisseur"
	1    2200 6600
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E3BAE52
P 2050 6450
F 0 "R?" V 2000 6450 50  0000 C BNN
F 1 "1k" V 2050 6450 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2050 6450 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 2050 6450 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 2050 6450 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 2050 6450 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 2050 6450 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 2050 6450 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 2050 6450 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 2050 6450 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 2050 6450 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 2050 6450 50  0001 C CNN "DeviceFournisseur"
	1    2050 6450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E3C769C
P 1900 6950
F 0 "#PWR?" H 1900 6700 50  0001 C CNN
F 1 "GND" H 1905 6777 50  0001 C CNN
F 2 "" H 1900 6950 50  0001 C CNN
F 3 "" H 1900 6950 50  0001 C CNN
	1    1900 6950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5E3C8F45
P 1100 6700
F 0 "SW?" V 1250 6850 50  0000 C CNN
F 1 "SW_Push" H 1100 6894 50  0001 C CNN
F 2 "" H 1100 6900 50  0001 C CNN
F 3 "~" H 1100 6900 50  0001 C CNN
	1    1100 6700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E3CAD07
P 1100 6950
F 0 "#PWR?" H 1100 6700 50  0001 C CNN
F 1 "GND" H 1105 6777 50  0001 C CNN
F 2 "" H 1100 6950 50  0001 C CNN
F 3 "" H 1100 6950 50  0001 C CNN
	1    1100 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 6900 1100 6950
Wire Wire Line
	1900 6900 1900 6950
Wire Wire Line
	1950 6450 1900 6450
Wire Wire Line
	1900 6450 1900 6500
Wire Wire Line
	1900 6450 1850 6450
Connection ~ 1900 6450
Wire Wire Line
	2150 6450 2200 6450
Wire Wire Line
	2200 6450 2200 6500
Wire Wire Line
	2200 6700 2200 6950
Wire Wire Line
	2200 6950 1900 6950
Connection ~ 1900 6950
Wire Wire Line
	1650 6450 1600 6450
Wire Wire Line
	2200 6450 2250 6450
Connection ~ 2200 6450
Wire Wire Line
	1900 6300 1900 6450
Text Label 1100 6200 0    50   ~ 0
A9G_RESET
Text Label 1600 6200 0    50   ~ 0
A9G_V_IO
Text Label 1900 6300 0    50   ~ 0
A9G_PWR_KEY
Text Label 2250 6450 0    50   ~ 0
A9G_IO29
Wire Wire Line
	1600 6200 1600 6450
Wire Wire Line
	1100 6200 1100 6500
Text Label 1350 7250 2    50   ~ 0
A9G_IO27
Text Label 1350 7450 2    50   ~ 0
A9G_IO28
$Comp
L Device:R_Small R?
U 1 1 5E4010C2
P 1750 7300
F 0 "R?" V 1700 7300 50  0000 C BNN
F 1 "1k" V 1750 7300 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1750 7300 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 1750 7300 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 1750 7300 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 1750 7300 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 1750 7300 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1750 7300 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 1750 7300 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 1750 7300 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 1750 7300 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1750 7300 50  0001 C CNN "DeviceFournisseur"
	1    1750 7300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E403B80
P 1750 7500
F 0 "R?" V 1700 7500 50  0000 C BNN
F 1 "1k" V 1750 7500 25  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1750 7500 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7F1773204-3%7F1%7Fpdf%7FEnglish%7FENG_DS_1773204-3_1.pdf%7F1-2176340-9" H 1750 7500 50  0001 C CNN
F 4 "TE CONNECTIVITY" H 1750 7500 50  0001 C CNN "Fabricant"
F 5 "CRGCQ0603J1K0" H 1750 7500 50  0001 C CNN "RefFabricant"
F 6 "https://www.te.com/usa-en/product-1-2176340-9.html" H 1750 7500 50  0001 C CNN "LinkFabricant"
F 7 "Farnell" H 1750 7500 50  0001 C CNN "Fournisseur"
F 8 "2861864" H 1750 7500 50  0001 C CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2861864" H 1750 7500 50  0001 C CNN "LinkFournisseur"
F 10 "0,02" H 1750 7500 50  0001 C CNN "CostFournisseur"
F 11 "EUR" H 1750 7500 50  0001 C CNN "DeviceFournisseur"
	1    1750 7500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E408EC6
P 1900 7350
F 0 "#PWR?" H 1900 7100 50  0001 C CNN
F 1 "GND" H 1905 7177 50  0001 C CNN
F 2 "" H 1900 7350 50  0001 C CNN
F 3 "" H 1900 7350 50  0001 C CNN
	1    1900 7350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E40B5A4
P 1900 7550
F 0 "#PWR?" H 1900 7300 50  0001 C CNN
F 1 "GND" H 1905 7377 50  0001 C CNN
F 2 "" H 1900 7550 50  0001 C CNN
F 3 "" H 1900 7550 50  0001 C CNN
	1    1900 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 7500 1900 7500
Wire Wire Line
	1900 7500 1900 7550
Wire Wire Line
	1850 7300 1900 7300
Wire Wire Line
	1900 7300 1900 7350
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E414201
P 1500 7300
F 0 "D?" H 1500 7443 50  0000 C CNN
F 1 "LED_Small_ALT" H 1500 7444 50  0001 C CNN
F 2 "" V 1500 7300 50  0001 C CNN
F 3 "~" V 1500 7300 50  0001 C CNN
	1    1500 7300
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED_Small_ALT D?
U 1 1 5E415424
P 1500 7500
F 0 "D?" H 1500 7643 50  0000 C CNN
F 1 "LED_Small_ALT" H 1500 7644 50  0001 C CNN
F 2 "" V 1500 7500 50  0001 C CNN
F 3 "~" V 1500 7500 50  0001 C CNN
	1    1500 7500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 7300 1650 7300
Wire Wire Line
	1400 7300 1350 7300
Wire Wire Line
	1400 7500 1350 7500
Wire Wire Line
	1350 7250 1350 7300
Wire Wire Line
	1350 7450 1350 7500
Wire Wire Line
	1650 7500 1600 7500
Text Label 2950 2250 0    50   ~ 0
A9G_RESET
Text Label 2950 2350 0    50   ~ 0
A9G_V_IO
Text Label 2950 2450 0    50   ~ 0
A9G_PWR_KEY
Text Label 2950 2550 0    50   ~ 0
A9G_IO29
$EndSCHEMATC
